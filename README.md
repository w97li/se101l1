# triangles
Function takes 3 x,y coordinates (6 inputs) and outputs the area of the triangle formed by the points.

Expected input: x1 y1 x2 y2 x3 y3
Expected output: The area of the triangle formed by points (x1, y1), (x2, y2), and (x3, y3) is: (value of the area)